/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.Nodo;
import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
public class ListaEstudiantesSE implements Serializable {

    private Nodo cabeza;

    public ListaEstudiantesSE() {
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public String adicionarNodo(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new Nodo(info);

        } else {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            //Parado en el último  
            temp.setSiguiente(new Nodo(info));
        }
        return "Adicionado con éxito";

    }

    public String adicionarNodoInicial(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new Nodo(info);

        } else {
            Nodo temp = new Nodo(info);
            temp.setSiguiente(cabeza);
            cabeza = temp;
        }
        return "Adicionado con éxito";

    }

    public String listarNodos() {
        String listado = "";
        if (cabeza == null) {
            return "La lista está vacía";
        } else {
            Nodo temp = cabeza;
            while (temp != null) {
                listado += temp.getDato();
                temp = temp.getSiguiente();
            }
            return listado;
        }

    }

    public int contarNodos() {
        if (cabeza == null) {
            return 0;
        } else {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();
            }
            return cont;
        }
    }

    public Nodo obtenerUltimoNodo() {
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }

    public int obtenerUltimoNodoEliminar() {
        if (cabeza != null) {
            int conta = 1;
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
                conta++;
            }
            return conta;
        }
        return 0;
    }

    public Nodo obtenerNodoxPosicion(int pos) {
        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                if (pos == cont) {

                    return new Nodo(temp.getDato());
                }
                temp = temp.getSiguiente();
                cont++;
            }
        }
        return null;
    }

    /////////////////
    public Nodo obtenerNodoInvertido(int pos) {
        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                if (pos == cont) {

                    return new Nodo(temp.getDato());
                }
                temp = temp.getSiguiente();
                cont++;
            }
        }
        return null;
    }

    int contador = 1;
    int contadorP = 1;

    public String paresImparesNodos() {
        String listado = " ";
        String listado2 = " ";
        if (cabeza == null) {
            return "La lista está vacía";
        } else {
            Nodo temp = cabeza;

            while (temp != null) {

                if (contador % 2 != 0) {
                    listado = listado + "Impar  ";
                    listado = listado + " " + contador;
                    listado += " " + temp.getDato();
                    temp = temp.getSiguiente();
                } else {
                    temp = temp.getSiguiente();
                }
                contador++;
            }

            Nodo temp1 = cabeza;

            while (temp1 != null) {

                if (contadorP % 2 != 0) {
                    temp1 = temp1.getSiguiente();
                } else {
                    listado2 = listado2 + "Par  ";
                    listado2 = listado2 + " " + contadorP;
                    listado2 += " " + temp1.getDato();
                    temp1 = temp1.getSiguiente();

                }
                contadorP++;
            }
            System.out.println(listado + listado2);
            contador = 1;
            contadorP = 1;
            return listado + listado2;
        }
    }

    public String eliminarNodoPorPosicion(int posicion) {
        if (posicion <= 0) {
            return "No existe nodo en esta posición.";
        } else if (posicion == 1) {

            cabeza = cabeza.getSiguiente();
            return "Nodo eliminado exitosamente.";
        } else {
            Nodo temp1 = cabeza;
            int cont = 1;
            while (temp1 != null && cont <= posicion) {
                if (cont == posicion - 1) {
                    Nodo temp2 = temp1;
                    temp1 = temp1.getSiguiente();
                    temp1 = temp1.getSiguiente();
                    temp2.setSiguiente(temp1);
                    temp1 = temp2;
                }
                cont++;
                temp1 = temp1.getSiguiente();
            }
            return "Nodo eliminado satisfactoriamente.";
        }

    }

}
