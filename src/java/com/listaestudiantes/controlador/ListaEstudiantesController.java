/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.Nodo;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
@Named(value = "listaEstudiantesController")
@SessionScoped
public class ListaEstudiantesController implements Serializable {

    private ListaEstudiantesSE lista = new ListaEstudiantesSE();

    private Nodo temp;

    private boolean verNuevo = false;

    private boolean verBuscar = false;

    private Estudiante estudianteAdicionar;

    private Estudiante estudianteEncontrado;

    private int posicionBuscar = 0;

    private String bloquear = "false";
  

    int contadorPos = 1;
    int contadorPosicion = 1;
    int conta = 0;

    private boolean listaInvertida = false;

    public boolean isListaInvertida() {
        return listaInvertida;
    }

    public void setListaInvertida(boolean listaInvertida) {
        this.listaInvertida = listaInvertida;
    }

    public String getBloquear() {
        return bloquear;
    }

    public void setBloquear(String bloquear) {
        this.bloquear = bloquear;
    }

    public int getPosicionBuscar() {
        return posicionBuscar;
    }

    public void setPosicionBuscar(int posicionBuscar) {
        this.posicionBuscar = posicionBuscar;
    }

    //////
    private int posicionEliminar = 1;

    public int getPosicionEliminar() {
        return posicionBuscar;
    }

    public void setPosicionEliminar(int posicionEliminar) {
        this.posicionEliminar = posicionEliminar;
    }

    ////
    public Estudiante getEstudianteEncontrado() {
        return estudianteEncontrado;
    }

    public void setEstudianteEncontrado(Estudiante estudianteEncontrado) {
        this.estudianteEncontrado = estudianteEncontrado;
    }

    public boolean isVerBuscar() {
        return verBuscar;
    }

    public void setVerBuscar(boolean verBuscar) {
        this.verBuscar = verBuscar;
    }

    public Estudiante getEstudianteAdicionar() {
        return estudianteAdicionar;
    }

    public void setEstudianteAdicionar(Estudiante estudianteAdicionar) {
        this.estudianteAdicionar = estudianteAdicionar;
    }

    public boolean isVerNuevo() {
        return verNuevo;
    }

    public void setVerNuevo(boolean verNuevo) {
        this.verNuevo = verNuevo;
    }

    public Nodo getTemp() {
        return temp;
    }

    public void setTemp(Nodo temp) {
        this.temp = temp;
    }

    private String listado;

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }

    public ListaEstudiantesSE getLista() {
        return lista;
    }

    public void setLista(ListaEstudiantesSE lista) {
        this.lista = lista;
    }

    /**
     * Creates a new instance of ListaEstudiantesController
     */
    public ListaEstudiantesController() {
        adicionarEstudiante("Mauricio López", "20");

        adicionarEstudiante("Junior Celis", "21");

        adicionarEstudiante("Sebastián", "20");

        adicionarEstudiante("Marlon", "27");
        //mostrarListado();

        temp = lista.getCabeza();
    }

    public void mostrarListado() {
        listado = lista.listarNodos();
    }

    public void adicionarEstudiante(String nombre, String edad) {
        Estudiante estu = new Estudiante(nombre, edad);

        lista.adicionarNodo(estu);

    }

    public void adicionarEstudianteAlInicio(String nombre,
            String edad) {
        Estudiante estu = new Estudiante(nombre, edad);

        lista.adicionarNodoInicial(estu);

    }

    public void irAlsiguiente() {

        if (listaInvertida == true) {

            if (conta == 1) {
                Nodo mostrar = lista.obtenerNodoInvertido(1);
                temp = mostrar;

            } else {

                int ultimoDe = lista.obtenerUltimoNodoEliminar();

                Nodo mostrar = lista.obtenerNodoInvertido(ultimoDe - contadorPosicion);

                conta = ultimoDe - contadorPosicion;

                temp = mostrar;
                contadorPosicion++;

            }

        } else {

            if (temp.getSiguiente() != null) {

                temp = temp.getSiguiente();
            } else {
                bloquear = "true";
                contadorPos = contadorPos - 1;
            }
            contadorPos++;

        }
    }

    public void anterior() {
        //Imposible
    }

    public void irAlPrimero() {
        if (listaInvertida == true) {
            Nodo ultimo = lista.obtenerUltimoNodo();
            int ultimate = lista.obtenerUltimoNodoEliminar();
            if (ultimo != null) {
                temp = ultimo;
                contadorPos = ultimate;

            }
            contadorPosicion = 1;
            conta = 0;
        } else {
            contadorPos = 1;
            temp = lista.getCabeza();
            bloquear = "false";
        }

    }

    public void irAlUltimo() {
        if (listaInvertida == true) {

            temp = lista.getCabeza();

        } else {

            Nodo ultimo = lista.obtenerUltimoNodo();
            int ultimate = lista.obtenerUltimoNodoEliminar();
            if (ultimo != null) {
                temp = ultimo;
                contadorPos = ultimate;

            }
        }
    }

    public void verCrearEstudiante() {
        verNuevo = true;
        estudianteAdicionar = new Estudiante();
    }

    public void adicionarAlInicio() {
        lista.adicionarNodoInicial(estudianteAdicionar);
        irAlPrimero();
        verNuevo = false;
    }

    public void adicionarAlFinal() {
        lista.adicionarNodo(estudianteAdicionar);
        irAlPrimero();
        verNuevo = false;
    }
    

    public void habilitarBuscar() {
        verBuscar = true;
    }

    public void buscarPorPosicion() {
        estudianteEncontrado = lista.obtenerNodoxPosicion(posicionBuscar).getDato();
    }

    public void eliminarNodoPorPosicion() {

        estudianteEncontrado = lista.obtenerNodoxPosicion(posicionEliminar).getDato();
        lista.eliminarNodoPorPosicion(posicionEliminar);
        irAlPrimero();

    }

    public void eliminarActual() {

        lista.eliminarNodoPorPosicion(contadorPos);

        if (contadorPos == 1) {

        } else {
            contadorPos = contadorPos - 1;
        }

        irAlPrimero();
        bloquear = "false";
    }
public void imparesYPares(){
     
   listado = lista.paresImparesNodos();
        
               
                
        
    }
    public void invertirLista() {

        Nodo ultimo = lista.obtenerUltimoNodo();
        int ultimate = lista.obtenerUltimoNodoEliminar();
        if (ultimo != null) {
            temp = ultimo;
            contadorPos = ultimate;

        }

        bloquear="false";
        listaInvertida = true;

    }

    public void listaOrden() {

        temp = lista.getCabeza();

        listaInvertida = false;

        contadorPosicion = 1;
            conta = 0;
        
    }

}
